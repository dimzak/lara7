<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProductController extends Controller
{
    //
    public function getIndex(){
        $product = \App\Product::all();
        return view('shop.index',['pros'=>$product]);
    }
}
