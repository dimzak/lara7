@extends('layout.master')

@section('title')
batu
@endsection

@section('content')
@foreach($pros->chunk(3) as $pds)
<div class="row">
    @foreach($pds as $pd)
    <div class="col-sm-6 col-md-4">
        <div class="thumbnail">
            <img class="img-responsive" src="{{$pd -> imagePath}}" alt="...">
            <div class="caption">
                <h3>{{$pd -> title}}</h3>
                <p class="desc">{{$pd -> desc}}</p>

                <div class="clearfix">
                    <div class="pull-left price">{{$pd -> price}}</div>
                    <a href="#" class="btn btn-success pull-right" role="button">Button</a>
                </div>
            </div>
        </div>
    </div>
    @endforeach
</div>
@endforeach
@endsection
